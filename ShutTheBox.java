public class ShutTheBox {
	public static void main(String[] args) {
		boolean gameOver = false;
		System.out.println("Hello! Welcome to my Shut The Box game! Enjoy!");
		Board game = new Board();
		while (!gameOver) {
			System.out.println("Player 1's turn");
			System.out.println(game.toString());
			if (game.playATurn()) {
				System.out.println("Player 2 wins!");
				gameOver = true;
			} else {
				System.out.println("Player 2's turn");
				System.out.println(game.toString());
				if (game.playATurn()) {
					System.out.println("Player 1 wins!");
					gameOver = true;
				}
			}
		}
	}
}