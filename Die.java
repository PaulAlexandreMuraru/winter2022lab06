import java.util.Random;

public class Die {
	private int pips;
	private Random rndDie;
	
	public Die() {
		pips = 1;
		rndDie = new Random();
	}
	
	public int getPips() {
		return this.pips;
	}
	
	public void roll() {
		this.pips = rndDie.nextInt(6) + 1;
	}
	
	public String toString() {
		return "You rolled a : " + this.pips + "!";
	}
}