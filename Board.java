public class Board {
	private Die die1;
	private Die die2;
	private boolean[] closedTiles;
	
	public Board() {
		die1 = new Die();
		die2 = new Die();
		closedTiles = new boolean[12];
	}
	
	public String toString() {
		String b = "";
		for (int i = 0; i < this.closedTiles.length; i++) {
			if (!this.closedTiles[i]) {
				b += (i + 1) + " ";
			} else {
				b += "X ";
			}
		}
		return b;
	}
	
	public boolean playATurn() {
		die1.roll();
		die2.roll();
		System.out.println(die1);
		System.out.println(die2);
		int sum = die1.getPips() + die2.getPips();
		if (!this.closedTiles[sum-1]) {
			this.closedTiles[sum-1] = true;
			System.out.println("Closing tile " + sum);
			return false;
		} else {
			System.out.println("Tile is already closed.");
			return true;
		}
	}
}